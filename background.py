import cairo  # type: ignore
import random


def _set_blue(context):
    context.set_source_rgb(14 / 256, 32 / 256, 86 / 256)


def _set_black(context):
    context.set_source_rgb(0, 0, 0)


def _set_color(context, is_blue):
    if is_blue:
        _set_black(context)
    else:
        _set_blue(context)


def _calc_seg_length(is_blue, heightv, row):
    if not is_blue:
        row = heightv - row
    divider = 24.0
    return max(0.0, row / divider + random.randrange(int(-row / divider), int(row / divider) + 1))


def make_background(filename):
    FinalInchesWide = 14.05
    FinalInchesTall = 9.26
    Dpi = 300
    HeightP = int(Dpi * FinalInchesTall)
    WidthP = int(Dpi * FinalInchesWide)

    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, WidthP, HeightP)
    context = cairo.Context(surface)
    _set_blue(context)

    HeightV = HeightP
    WidthV = WidthP

    context.scale(WidthP / WidthV, HeightP / HeightV)

    context.set_line_width(1.9)

    divider = 4.0

    is_blue = True
    for row in range(0, int(HeightV), 1):
        x_pos = -(WidthV / divider)
        while x_pos < WidthV:
            _set_color(context, is_blue)
            context.move_to(x_pos, row)
            x_pos += _calc_seg_length(is_blue, HeightV, row)
            context.line_to(x_pos, row)
            context.stroke()
            is_blue = not is_blue

    surface.write_to_png(filename)


if __name__ == "__main__":
    import sys
    sys.exit(make_background(sys.argv[1]))
