import math
from matplotlib import colors  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
from matplotlib.collections import LineCollection  # type: ignore
import networkx as nx  # type: ignore
from networkx.algorithms.shortest_paths.unweighted import (single_source_shortest_path_length as  # type:ignore
                                                           path_length)
import numpy as np  # type: ignore
from scipy.misc import imread  # type: ignore

n_rows = 94
n_cols = 141
INCHES_WIDE = 14.05
INCHES_TALL = 9.26
MARKER_SIZE = INCHES_WIDE / n_cols * 32.0

EDGE_PROB = 0.30
DPI = 300.0

# The coordinates of the 3 nodes that comprise the V.
v_base_c = n_cols // 2
v_base_r = n_rows // 2 - n_rows // 4
v_width = n_cols // 6
v_height = v_width * 2
v_rl_r = v_base_r + v_height
v_right_c, v_left_c = (v_base_c + v_width), (v_base_c - v_width)

# Fudge a little due to coordinates being at center of nodes
left_bound = -.32
right_bound = n_cols - 0.1
bottom_bound = -.3
top_bound = n_rows - .62

LINEWIDTH = 0.7

# halo around the V nodes and edges
halo_size = 2
halo_alpha = 0.30

VERTEX_GREEN = (0., 160 / 256., 0.)  # FIXME
VERTEX_GRAY = (169 / 256., 169 / 256., 169 / 256.)  # FIXME
HALO_COLOR = (0., 1., 0.)


def make_nodes(G):
    """ Makes nodes in a grid """
    for r in range(n_rows):
        for c in range(n_cols):
            y_pos = r
            x_pos = c + 0.5 * (r % 2)
            G.add_node((c, r), pos=(x_pos, y_pos), special=False)


def make_v(G):
    """ Make the V """
    G.nodes[(v_base_c, v_base_r)]['special'] = True
    G.nodes[(v_right_c, v_rl_r)]['special'] = True
    G.nodes[(v_left_c, v_rl_r)]['special'] = True
    G.add_edge((v_base_c, v_base_r), (v_right_c, v_rl_r), special=True)
    G.add_edge((v_base_c, v_base_r), (v_left_c, v_rl_r), special=True)


def delete_nodes_around_v(G):
    """ Delete all nodes that get in the way of the V"""
    for c in range(v_base_c + 1, v_base_c + v_width):
        r = (c - v_base_c) * 2 + v_base_r
        G.remove_node((c, r))
        G.remove_node((c - 1, r - 1))
    G.remove_node((v_base_c + v_width - 1, v_rl_r - 1))

    for c in range(v_base_c - 1, v_base_c - v_width, -1):
        r = (v_base_c - c) * 2 + v_base_r
        G.remove_node((c, r))
        G.remove_node((c, r - 1))
    G.remove_node((v_base_c - v_width, v_rl_r - 1))


def connect_nodes(G):
    """ Connect the nodes randomly """
    for r in range(n_rows):
        for c in range(n_cols):
            probs = np.random.random(3)
            # prob edge to right
            my_node = (c, r)
            if my_node not in G.nodes:
                continue
            in_odd_row = (r % 2 == 1)
            node_right = (c + 1, r)
            node_above_left = (c if in_odd_row else c - 1, r + 1)
            node_above_right = (c + 1 if in_odd_row else c, r + 1)
            added_edge = False
            if probs[0] < EDGE_PROB and node_right in G.nodes:
                G.add_edge(my_node, node_right)
                added_edge = True
            if probs[1] < EDGE_PROB and node_above_left in G.nodes:
                G.add_edge(my_node, node_above_left)
                added_edge = True
            if probs[2] < EDGE_PROB and node_above_right in G.nodes:
                G.add_edge(my_node, node_above_right)
                added_edge = True
            # Add an edge if we haven't yet
            # (This backup node logic introduces a subtle V bias)
            backup_node = node_above_left if c < v_base_c else node_above_right
            if not added_edge and backup_node in G.nodes:
                G.add_edge(my_node, backup_node)


def go_left(c, r):
    return c - 1, r


def go_right(c, r):
    return c + 1, r


def go_above_left(c, r):
    in_odd_row = (r % 2 == 1)
    return c if in_odd_row else c - 1, r + 1


def go_above_right(c, r):
    in_odd_row = (r % 2 == 1)
    return (c + 1 if in_odd_row else c, r + 1)


def go_below_left(c, r):
    in_odd_row = (r % 2 == 1)
    return c if in_odd_row else c - 1, r - 1


def go_below_right(c, r):
    in_odd_row = (r % 2 == 1)
    return c + 1 if in_odd_row else c, r - 1


def set_edges_around_v(G):
    """Add all edges around the tops of the V and remove around the base """
    special_nodes = [name for name, props in G.nodes.items() if props['special']]
    for c, r in special_nodes:
        target_nodes = (go_left(c, r), go_right(c, r), go_above_left(c, r), go_above_right(c, r),
                        go_below_left(c, r), go_below_right(c, r))
        for target in target_nodes:
            if target not in G:
                continue
            if c == v_base_c:
                try:
                    G.remove_edge((c, r), target)
                except nx.exception.NetworkXError:
                    pass
            else:
                if target[1] <= r:
                    try:
                        G.remove_edge((c, r), target)
                    except nx.exception.NetworkXError:
                        pass
                    continue
                G.add_edge((c, r), target)


def eliminate_islands(G):
    """ Make minimum number of connections to connect disconnected groups """
    for i in range(100):
        components = list(nx.connected_components(G))
        len_c = len(components)
        if len_c == 1:
            print('Got down to 1 component')
            break
        print('components left: ', len_c)
        smallest_component = min(components, key=len)
        for c, r in smallest_component:
            added = False
            target_nodes = (go_left(c, r), go_right(c, r), go_above_left(c, r),
                            go_above_right(c, r), go_below_left(c, r), go_below_right(c, r))
            for c2, r2 in target_nodes:
                if (c2, r2) not in G.nodes:
                    continue
                # Skip if we're already connected
                if (c2, r2) in smallest_component:
                    continue
                G.add_edge((c, r), (c2, r2))
                print('Adding (%d, %d) -> (%d, %d)' % (c, r, c2, r2))
                added = True
                break
            if added:
                break


def dist_to_alpha(dist):
    """ Take distance as a path distance and convert to the alpha a node is drawn in """
    MIN_ALPHA = 0.01
    POWER = 2.0
    MAX_DIST = 160.0
    dist = min(dist, MAX_DIST) + 1 if dist > 0 else 0
    return max(MIN_ALPHA, (MAX_DIST - dist) ** POWER / MAX_DIST ** POWER)


def find_distances(G):
    ''' Calculates for each node the shortest path to closest of the V nodes '''
    special_nodes = [name for name, props in G.nodes.items() if props['special']]
    distances = [path_length(G, n) for n in special_nodes]
    DEFAULT_DIST = 9999
    for node in G.nodes:
        G.nodes[node]['dist'] = min(d.get(node, DEFAULT_DIST) for d in distances)


def make_axes():
    plt.axis('off')
    fig = plt.gcf()
    fig.set_size_inches(INCHES_WIDE, INCHES_TALL)
    fig.set_facecolor('black')
    ax = fig.gca()
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    return ax


def add_background(plt, background_fn):
    img = imread(background_fn)
    plt.imshow(img, zorder=0, aspect='auto', extent=[left_bound, right_bound, bottom_bound, top_bound])


def draw_v(G, ax):
    special_mult = 1.9
    # Node Halo
    node_halo_size = (MARKER_SIZE + LINEWIDTH) * special_mult + halo_size / 2.0
    pos = nx.get_node_attributes(G, 'pos')
    xy_special = np.asarray([pos[v] for v in list(G) if G.nodes[v]['special']])

    ax.plot(xy_special[:, 0], xy_special[:, 1], 'o', markerfacecolor='none', color=HALO_COLOR,
            markeredgewidth=halo_size / 2.0,
            markersize=node_halo_size, alpha=halo_alpha, zorder=999)

    # V Nodes
    ax.plot(xy_special[:, 0], xy_special[:, 1],
            'o',
            markerfacecolor=VERTEX_GRAY,
            color=VERTEX_GREEN,
            markeredgewidth=LINEWIDTH * special_mult,
            markersize=MARKER_SIZE * special_mult)
    # V edges
    # V edge halo
    nx.draw_networkx_edges(G, pos=pos,
                           edgelist=[e for e in G.edges if G.edges[e].get('special', False)],
                           edge_color=colors.to_hex(HALO_COLOR),
                           width=LINEWIDTH * special_mult + halo_size, alpha=halo_alpha)
    nx.draw_networkx_edges(G, pos=pos, edgelist=[e for e in G.edges if G.edges[e].get('special', False)],
                           edge_color=colors.to_hex(VERTEX_GREEN), width=LINEWIDTH * special_mult)


def draw_regular_nodes(G, ax):
    # The other regular nodes
    for _, attrs in G.nodes.items():
        if attrs['special']:
            continue
        c = plt.Circle(attrs['pos'], 0.25, facecolor='none', edgecolor=colors.to_hex(VERTEX_GREEN),
                       linewidth=LINEWIDTH,
                       alpha=dist_to_alpha(attrs['dist']))
        ax.add_artist(c)


def calc_segments_alphas(G):
    """ The edge segments shall not penetrate inside the nodes """
    segments = []
    alphas = []
    r = .29  # determined empirically to minimally overlap with node circle
    for e in G.edges:
        if 'special' in G.edges[e]:
            continue
        node0, node1 = G.nodes[e[0]], G.nodes[e[1]]
        x1, y1 = node0['pos']
        x2, y2 = node1['pos']
        theta = math.atan2(y2 - y1, x2 - x1)
        delta_x = r * math.cos(theta)
        delta_y = r * math.sin(theta)
        segments.append(((x1 + delta_x, y1 + delta_y), (x2 - delta_x, y2 - delta_y)))
        alpha = dist_to_alpha((node0['dist'] + node1['dist']) / 2)
        alphas.append(alpha)
    return segments, alphas


def set_draw_limits():
    plt.xlim(left_bound, right_bound)
    plt.ylim(bottom_bound, top_bound)


def draw_regular_edges(G, ax):
    segments, alphas = calc_segments_alphas(G)
    line_segments = LineCollection(segments, linewidth=LINEWIDTH, colors=[(*VERTEX_GREEN, a) for a in alphas])
    ax.add_collection(line_segments)


def save_drawing(filename):
    plt.savefig(filename, dpi=DPI)


def main():
    G = nx.Graph()
    make_nodes(G)
    make_v(G)
    delete_nodes_around_v(G)
    connect_nodes(G)

    set_edges_around_v(G)
    eliminate_islands(G)
    find_distances(G)

    ax = make_axes()
    add_background(plt, 'blue_to_black.png')
    draw_v(G, ax)
    draw_regular_nodes(G, ax)
    draw_regular_edges(G, ax)
    set_draw_limits()
    save_drawing('front.png')


if __name__ == "__main__":
    main()
